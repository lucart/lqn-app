package com.android.lqn.lqn.restApi;

/**
 * Created by augustopinto on 8/8/17.
 */

public interface IUpdateOfferAction {
    void onSuccess();
    void onFail(String message);
}
