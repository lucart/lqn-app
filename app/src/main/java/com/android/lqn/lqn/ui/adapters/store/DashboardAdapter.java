package com.android.lqn.lqn.ui.adapters.store;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.model.Offer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by augustopinto on 7/25/17.
 */

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.ViewHolder> {

    private Context mContext;
    private List<Offer> mOfferList;
    private onDeleteOfferListener mDeleteClickListener;
    private onEditOfferListener mEditClickListener;

    public interface onEditOfferListener {
        void onItemSelected(int position);
    }

    public interface onDeleteOfferListener {
        void onItemDeleted(int position);
    }

    public DashboardAdapter(Context context) {
        mContext = context;
        mOfferList = new ArrayList<>();
    }

    public DashboardAdapter(Context context, List<Offer> offerList) {
        mContext = context;
        mOfferList = offerList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.dashboard_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,  int position) {
        // Fill the view with data
        holder.mOfferHeader.setText(mContext.getResources().getString(R.string.dashboard_amount_reserved_offers,
                String.valueOf(mOfferList.get(position).getAmountReserved())));

        holder.mTitle.setText(mOfferList.get(position).getName());
        holder.mLqnPoints.setText(String.valueOf(mOfferList.get(position).getLqnPoints()));
        Picasso.with(mContext).load(mOfferList.get(position).getImageUrl()).into(holder.mImage);
        holder.mOfferType.setText(getOfferTypeName(mOfferList.get(position)));
        int disc = (int) mOfferList.get(position).getRefoundPercent();
        holder.mRefund.setText(mContext.getString(R.string.dashboard_discount_percentage,
                String.valueOf(disc)));

        holder.mDeleteProgress.setVisibility(View.GONE);
        holder.mDeleteIcon.setVisibility(View.VISIBLE);

        holder.mDeleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mDeleteProgress.setVisibility(View.VISIBLE);
                holder.mDeleteIcon.setVisibility(View.GONE);
                mDeleteClickListener.onItemDeleted(holder.getAdapterPosition());
            }
        });

        holder.mEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditClickListener.onItemSelected(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOfferList.size();
    }

    public void setDeleteIconClickListener(onDeleteOfferListener listener) {
        mDeleteClickListener = listener;
    }

    public void setEditButtonClickListener(onEditOfferListener listener) {
        mEditClickListener = listener;
    }

    /**
     * This method adds the elements from the list sent by params at the end of the Adapter list.
     * For instance, if the Adapter list has 4 elements and a list sent by params has 3 elements
     * then the Adapter list will have 7 elements at the end of this method.
     * @param offers
     */
    public void updateOfferList(List<Offer> offers) {
        mOfferList.addAll(offers);
        notifyDataSetChanged();
    }

    /**
     * This method replace the elements in the Adapter list with the elements in the list sent by
     * params.
     * For instance, if the Adapter list has 3 elements and a list sent by params has 1 element
     * then the Adapter list will have just 1 element when this method finish.
     * @param offers
     */
    public void setOfferList(List<Offer> offers) {
        mOfferList = offers;
        notifyDataSetChanged();
    }

    /**
     * Returns the current Adapter list.
     * @return
     */
    public List<Offer> getOfferList() {
        return mOfferList;
    }

    private String getOfferTypeName(Offer offer) {
        String []types = mContext.getResources().getStringArray(R.array.offer_types_dashboard);
        String name = types[offer.getOfferType().getId()];
        return name;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        //Offer header shows how many customers want it.
        public TextView mOfferHeader;
        public TextView mTitle;
        public TextView mLqnPoints;
        public TextView mRefund;
        public TextView mOfferType;
        public ImageView mImage;
        public ImageView mDeleteIcon;
        public ProgressBar mDeleteProgress;
        public Button mEditButton;

        public ViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            initViews();
        }

        private void initViews() {
            mOfferHeader = (TextView) mView.findViewById(R.id.offer_header);
            mTitle = (TextView) mView.findViewById(R.id.offer_title);
            mLqnPoints = (TextView) mView.findViewById(R.id.lqn_points);
            mRefund = (TextView) mView.findViewById(R.id.refund);
            mOfferType = (TextView) mView.findViewById(R.id.offer_type);
            mImage = (ImageView) mView.findViewById(R.id.offer_image);
            mDeleteIcon = (ImageView) mView.findViewById(R.id.delete_offer_button);
            mDeleteProgress = (ProgressBar) mView.findViewById(R.id.delete_loading);
            mEditButton = (Button) mView.findViewById(R.id.edit_button);

            if (mDeleteProgress != null) {
                mDeleteProgress.setIndeterminate(true);
                mDeleteProgress.getIndeterminateDrawable().setColorFilter(0x2D2E41, //2D2E41 is the main color
                        PorterDuff.Mode.SRC_ATOP);
            }
        }

    }

}
