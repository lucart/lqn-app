package com.android.lqn.lqn.controller;

import android.util.Log;
import android.widget.Toast;

import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.model.ValidatedOffer;
import com.android.lqn.lqn.restApi.IDeleteOfferAction;
import com.android.lqn.lqn.restApi.IGetOfferListResponse;
import com.android.lqn.lqn.restApi.IShopActions;
import com.android.lqn.lqn.restApi.IValidatedOfferResponse;
import com.android.lqn.lqn.restApi.StoreService;
import com.android.lqn.lqn.utils.LogUtils;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This controller should be responsible for the following actions:
 *      - Upload a new offer
 *      - Get the whole offer list for a specific store
 * Created by augustopinto on 6/29/17.
 */

public class StoreActionsController {

    private static String TAG = StoreActionsController.class.getSimpleName();

    private static StoreActionsController mInstance;
    private Retrofit mRestApiCore;

    private StoreActionsController() {
        initRestApiCore();
    }

    public static StoreActionsController getInstance() {
        if (mInstance == null) {
            mInstance = new StoreActionsController();
        }
        return  mInstance;
    }

    private void initRestApiCore() {
        Gson gson = new GsonBuilder().create();

        mRestApiCore = new Retrofit.Builder().baseUrl(ServiceConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /**
     * Upload a new offer and saves it in the server.
     * @param offer - The com.google.gson.JsonObject object that maps an offer.
     * @param listener - The listener that indicates if the offer was inserted or not.
     */
    public void uploadNewOffer(JsonObject offer, final IShopActions listener) {
        StoreService storeService = mRestApiCore.create(StoreService.class);

        Call<JsonObject> call = storeService.uploadOffer(offer);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                Log.i(TAG, "Response code: " + response.code());
                if (response.code() == ServiceConstants.NO_ERROR) {
                    Log.i(TAG, "Call: " + call.request().toString());
                    Log.i(TAG, "Response message: " + response.message());
                    Log.i(TAG, response.toString());
                    listener.onSuccess();
                } else {
                    listener.onFail("WebService Error. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFail(call.toString());
            }
        });

    }

    /**
     * Calls to the server and request the whole offers for a specific store
     * @param id - The store id.
     * @param listener - The listener that indicates if the offer was inserted or not.
     */
    public void getAllOffers(int page, int id, final IGetOfferListResponse listener) {
        StoreService storeService = mRestApiCore.create(StoreService.class);
        Call<List<Offer>> call = storeService.getOfferList(page, id);
        call.enqueue(new Callback<List<Offer>>() {
            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    Log.i(TAG, response.toString());
                    Log.d(TAG, "Body: " + response.body().toString());
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail("WebService Error. Code: " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                listener.onFail("WebService Error");
            }
        });
    }

    /**
     * Makes a call to /deleteOffer/offerId
     * This endpoint delete a store offer.
     * @param id the offer id to delete.
     * @param listener
     */
    public void deleteOffer(int id, final IDeleteOfferAction listener) {
        StoreService storeService = mRestApiCore.create(StoreService.class);
        Call<JsonObject> call = storeService.deleteOffer(id);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    listener.onSuccess();
                } else {
                    listener.onFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFail(t.getMessage());
            }
        });
    }

    /**
     * Makes a call to /updateOffer/ and sends a Json in the body. The json represents the offer
     * to be updated. The json should contain only the fields to update. It is not necessary to
     * send fields that won't be updated.
     * @param offer The offer fields to update.
     * @param listener
     */
    public void updateOffer(JsonObject offer, final IShopActions listener) {
        StoreService storeService = mRestApiCore.create(StoreService.class);
        Call<JsonObject> call = storeService.updateOffer(offer);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    listener.onSuccess();
                } else {
                    listener.onFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onFail(t.getMessage());
            }
        });
    }

    /**
     * Makes a call to /findOfferByHashCode/hashCode/userId
     * This endpoint validates a hashCode and returns an offer if there was a customer that marked
     * the offer as wanted.
     * @param hashCode
     * @param userId
     * @param listener
     */
    public void findOfferByHashCode(String hashCode, String userId, final IValidatedOfferResponse listener) {
        StoreService storeService = mRestApiCore.create(StoreService.class);
        Call<ValidatedOffer> call = storeService.findOfferByHash(hashCode, userId);
        call.enqueue(new Callback<ValidatedOffer>() {
            @Override
            public void onResponse(Call<ValidatedOffer> call, Response<ValidatedOffer> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<ValidatedOffer> call, Throwable t) {
                Log.e(TAG, "Fail at find offer by hash");
                listener.onFail(t.getMessage());
            }
        });
    }

}
