package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.Offer;

import java.util.List;

/**
 * Created by augustopinto on 7/25/17.
 */

public interface IGetOfferListResponse {
    void onSuccess(List<Offer> offerList);
    void onFail(String message);
}
