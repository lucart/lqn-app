package com.android.lqn.lqn.utils;

import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by augustopinto on 8/30/17.
 */

public class BasicUtils {

    /**
     * @return the days left to expire the offer
     */
    public static int getExpirationDaysLeft(long epochTime) {
        Date expDayd = new Date();
        expDayd.setTime(epochTime * 1000);

        Date nowd = new Date();

        long dif = expDayd.getTime() - nowd.getTime();

        return (int) TimeUnit.DAYS.convert(dif, TimeUnit.MILLISECONDS);
    }

}
