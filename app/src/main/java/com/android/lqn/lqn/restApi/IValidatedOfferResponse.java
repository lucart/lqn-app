package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.ValidatedOffer;

/**
 * Created by augustopinto on 11/26/17.
 */

public interface IValidatedOfferResponse {

    void onSuccess(ValidatedOffer validatedOffer);
    void onFail(String message);

}
