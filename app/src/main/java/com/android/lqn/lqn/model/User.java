package com.android.lqn.lqn.model;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by augustopinto on 5/19/17.
 */

public class User implements Serializable {

    @SerializedName(ServiceConstants.ID_ATTRIBUTE)
    private int mId;

    @SerializedName(ServiceConstants.USERNAME_ATTRIBUTE)
    private String mUsername;

    @SerializedName(ServiceConstants.PASSWORD_ATTRIBUTE)
    private String mPassword;

    @SerializedName(ServiceConstants.USER_TYPE_ATTRIBUTE)
    private UserType mUserType;

    @SerializedName(ServiceConstants.SHOP_ATTRIBUTE)
    private Store mStore;

    public User(int id, String username, String password, UserType userType, Store store) {
        mId = id;
        mUsername = username;
        mPassword = password;
        mUserType = userType;
        mStore = store;
    }

    public User() {
        this(-1, "", "", new UserType(), new Store());
    }

    public User(JSONObject user) {
        parseToObject(user);
    }
    public int getId() {
        return mId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        this.mPassword = password;
    }

    public UserType getUserType() {
        return mUserType;
    }

    public Store getStore() {
        return mStore;
    }

    public void setStore(Store mStore) {
        this.mStore = mStore;
    }

    public JSONObject parseToJson() {
        JSONObject user = new JSONObject();
        try {
            user.put(ServiceConstants.ID_ATTRIBUTE, mId);
            user.put(ServiceConstants.USERNAME_ATTRIBUTE, mUsername);
            user.put(ServiceConstants.PASSWORD_ATTRIBUTE, mPassword);
            if (mUserType != null) {
                user.put(ServiceConstants.USER_TYPE_ATTRIBUTE, mUserType.parseToJson());
            }
            if (mStore != null) {
                user.put(ServiceConstants.SHOP_ATTRIBUTE, mStore.parseToJson());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return user;
    }

    private void parseToObject(JSONObject json) {
        try {
            if (json.has(ServiceConstants.ID_ATTRIBUTE)) {
                mId = json.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.USERNAME_ATTRIBUTE)) {
                mUsername = json.getString(ServiceConstants.USERNAME_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.PASSWORD_ATTRIBUTE)) {
                mPassword = json.getString(ServiceConstants.PASSWORD_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.USER_TYPE_ATTRIBUTE)) {
                mUserType = new UserType(json.getJSONObject(ServiceConstants.USER_TYPE_ATTRIBUTE));
            }
            if (json.has(ServiceConstants.SHOP_ATTRIBUTE)) {
                mStore = new Store(json.getJSONObject(ServiceConstants.SHOP_ATTRIBUTE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
