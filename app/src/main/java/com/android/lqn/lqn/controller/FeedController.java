package com.android.lqn.lqn.controller;

import android.util.Log;

import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.restApi.FeedService;
import com.android.lqn.lqn.restApi.IOffersLoaded;
import com.android.lqn.lqn.restApi.IShopActions;
import com.android.lqn.lqn.utils.LogUtils;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This controller should be responsible for the following actions:
 *      - Get the offers displayed in the feed by the user.
 *
 * Created by augustopinto on 4/4/17.
 */

public class FeedController {

    private static String TAG = FeedController.class.getSimpleName();

    private static FeedController mInstance;

    private Retrofit mRestApiCore;
    private FeedService mFeedService;

    private FeedController() {
        initRestApiCore();
    }

    public static FeedController getInstance() {
        if (mInstance == null) {
            mInstance = new FeedController();
        }
        return mInstance;
    }

    private void initRestApiCore() {
        Gson gson = new GsonBuilder().create();

        mRestApiCore = new Retrofit.Builder().baseUrl(ServiceConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();

        mFeedService = mRestApiCore.create(FeedService.class);
    }

    /**
     * Makes a call to /getOffers/page/offerTypeId
     * This endpoint returns an Offer list.
     * @param page the page to be returned from the WS
     * @param offerTypeId
     * @param listener
     */
    public void getOffers(int page, int offerTypeId, final IOffersLoaded listener) {
        Call<List<Offer>> call = mFeedService.getOffers(page, offerTypeId);
        call.enqueue(new Callback<List<Offer>>() {
            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail();
                }
            }

            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                Log.e(TAG, "onFailure getting the offers in FeedController");
                listener.onFail();
            }
        });
    }

    /**
     * Makes a call to /reserveOffer/offerId/userId/
     * This endpoint returns just a code to know if an offer was reserved.
     * @param offerId the offer id's to reserve
     * @param userId the used id's that want to reserve the offer
     * @param listener
     */
    public void reserveOffer(int offerId, int userId, final IShopActions listener) {
        Call<JsonObject> call = mFeedService.reserveOffer(offerId, userId);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    listener.onSuccess();
                } else {
                    listener.onFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.e(TAG, "onFailure reserving an offer");
                listener.onFail(t.getMessage());
            }
        });
    }

    /**
     * Makes a call to /getOffersByUser/page/userId/
     * This endpoint returns an offer list. The list has only offers that the user have marked as wanted.
     * @param page
     * @param userId
     * @param listener
     */
    public void getOffersByUser(int page, int userId, final IOffersLoaded listener) {
        Call<List<Offer>> call = mFeedService.getOffersByUser(page, userId);
        call.enqueue(new Callback<List<Offer>>() {
            @Override
            public void onResponse(Call<List<Offer>> call, Response<List<Offer>> response) {
                LogUtils.logResponseMessage(TAG, call, response);
                if (response.code() == ServiceConstants.NO_ERROR) {
                    listener.onSuccess(response.body());
                } else {
                    listener.onFail();
                }
            }

            @Override
            public void onFailure(Call<List<Offer>> call, Throwable t) {
                listener.onFail();
            }
        });
    }

}
