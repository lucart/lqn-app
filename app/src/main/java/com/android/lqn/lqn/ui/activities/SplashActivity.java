package com.android.lqn.lqn.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.ui.activities.consumer.UserHomeActivity;
import com.android.lqn.lqn.ui.activities.store.StoreHomeActivity;
import com.android.lqn.lqn.ui.activities.store.UploadNewOfferActivity;
import com.android.lqn.lqn.utils.ServiceConstants;

import org.json.JSONException;

/**
 * Created by augustopinto on 5/16/17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        if (LoginController.getInstance(getApplicationContext()).isLogged()) {
            int userType = -1;
            try {
                userType = LoginController.getInstance(getApplicationContext())
                        .getUserInfo().getUserType().getId();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (userType == ServiceConstants.SHOP_USER) {
                startDashboardActivity();
            } else if (userType == ServiceConstants.NEW_USER) {
                startFeedActivity();
            }
        } else {
            startLoginActivity();
        }
    }

    private void startLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void startFeedActivity() {
        Intent intent = new Intent(this, UserHomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void startUploadOfferActivity() {
        Intent intent = new Intent(this, UploadNewOfferActivity.class);
        startActivity(intent);
        finish();
    }

    private void startDashboardActivity() {
        Intent intent = new Intent(this, StoreHomeActivity.class);
        startActivity(intent);
        finish();
    }
}
