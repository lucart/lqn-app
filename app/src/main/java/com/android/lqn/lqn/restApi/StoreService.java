package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.model.ValidatedOffer;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by augustopinto on 6/28/17.
 */

public interface StoreService {

    @POST(ServiceConstants.UPLOAD_OFFERS)
    Call<JsonObject> uploadOffer(@Body JsonObject offer);

    @GET(ServiceConstants.GET_OFFER_BY_ID)
    Call<List<Offer>> getOfferList(@Path("page") int page, @Path("storeId") int storeId);

    @GET(ServiceConstants.DELETE_OFFER)
    Call<JsonObject> deleteOffer(@Path("offerId") int offerId);

    @POST(ServiceConstants.UPDATE_OFFER)
    Call<JsonObject> updateOffer(@Body JsonObject offer);

    @GET(ServiceConstants.FIND_OFFER_BY_HASH)
    Call<ValidatedOffer> findOfferByHash(@Path("hashCode") String hashCode, @Path("userId") String userId);
}
