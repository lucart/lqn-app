package com.android.lqn.lqn.model;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by augustopinto on 11/26/17.
 */

public class ValidatedOffer implements Serializable {

    @SerializedName(ServiceConstants.OFFER_VALIDATED_ATTRIBUTE)
    private Offer mOffer;

    @SerializedName(ServiceConstants.USER_VALIDATED_ATTRIBUTE)
    private User mUser;

    public ValidatedOffer(JSONObject json) {
        parseToObject(json);
    }

    public ValidatedOffer() {
        mOffer = new Offer();
        mUser = new User();
    }

    public Offer getOffer() {
        return mOffer;
    }

    public void setOffer(Offer mOffer) {
        this.mOffer = mOffer;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User mUser) {
        this.mUser = mUser;
    }

    private void parseToObject(JSONObject json) {
        try {
            if (json.has(ServiceConstants.OFFER_VALIDATED_ATTRIBUTE)) {
                mOffer = new Offer(json.getJSONObject(ServiceConstants.OFFER_VALIDATED_ATTRIBUTE));
            }
            if (json.has(ServiceConstants.USER_VALIDATED_ATTRIBUTE)) {
                mUser = new User(json.getJSONObject(ServiceConstants.USER_VALIDATED_ATTRIBUTE));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject parseToJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put(ServiceConstants.OFFER_VALIDATED_ATTRIBUTE, mOffer.parseToJSON());
            json.put(ServiceConstants.USER_VALIDATED_ATTRIBUTE, mUser.parseToJson());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }
}
