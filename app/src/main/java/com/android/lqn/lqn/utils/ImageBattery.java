package com.android.lqn.lqn.utils;

import java.util.Random;

/**
 * Created by augustopinto on 10/5/17.
 */

public class ImageBattery {

    private static String[] clothesImages = {
            "https://userscontent2.emaze.com/images/5a4a705d-6de4-4695-a634-9b5929c9504e/234d49a24bbdc13034146843c546ae26.jpg",
            "https://i.pinimg.com/736x/52/ec/a9/52eca97d5c141c7d8889d4e1905dbb5e--hipster-clothing-retro-clothing.jpg",
            "https://mensandbeauty.com/wp-content/uploads/2016/05/zapatillas-hombre-moda-new-balance.jpg",
            "http://www.icasamotos.com.ar/1341/zapatillas-fox-motion-transfer-talle-95.jpg",
            "https://d26lpennugtm8s.cloudfront.net/stores/005/695/products/zipper-peru-1-bdbe224a5cf277ac74a8115ff7f1cd5b-1024-1024.jpg",
            "http://cdn1.coppel.com/images/catalog/pr/8079192-1.jpg",
            "https://shop.r10s.jp/wassup/cabinet/robot-32/levis-505-0217-a.jpg",
            "https://ropasusada.com/ropas/jeans-de-marca-levis.jpg",
            "https://shop.r10s.jp/freshbox/cabinet/pants4/levis505_01n.jpg",
            "http://www.opensports.com.ar/media/catalog/product/cache/1/image/1200x/9df78eab33525d08d6e5fb8d27136e95/6/3/63118_0_6.jpg"
    };

    private static String[] techImages = {
            "https://pisces.bbystatic.com/BestBuy_US/store/ee/2016/mob/pr/153114-dept-page/cell_phones_unlocked.jpg;maxHeight=288;maxWidth=520",
            "https://www.androidcentral.com/sites/androidcentral.com/files/styles/large/public/topic_images/2014/Cell-Phone-Plans-topic-page-graphic.png?itok=QMawtS_h",
            "https://pisces.bbystatic.com/BestBuy_US/store/ee/2017/mob/flx/flx_0323_sol10807-mate9.jpg;maxHeight=291;maxWidth=291",
            "https://assets.pcmag.com/media/images/396774-the-10-best-ultraportables.jpg?width=410&height=230",
            "https://pisces.bbystatic.com/BestBuy_US/images/products/5723/5723427ld.jpg;maxHeight=460;maxWidth=460",
            "http://ssl-product-images.www8-hp.com/digmedialib/prodimg/lowres/c05392381.png",
            "https://cnet3.cbsistatic.com/img/MXzJ-rTNsjr3sqDW_KG8c44glgU=/770x433/2017/06/07/9f713b6e-6725-4ae7-b3ee-71518d156653/microsoft-surface-book-pro-003.jpg",
            "https://cnet1.cbsistatic.com/img/TTvUbygen_5eUjT8qNeQSsvRjV4=/770x433/2017/05/02/361e6b92-9af8-4e21-a6fc-350147744362/microsoft-surface-laptop-030.jpg",
            "https://i5.walmartimages.com/dfw/4ff9c6c9-2ac9/k2-_ed8b8f8d-e696-4a96-8ce9-d78246f10ed1.v1.jpg"
    };

    private static String[] foodImages = {
            "https://ichef-1.bbci.co.uk/news/660/cpsprodpb/1325A/production/_88762487_junk_food.jpg",
            "http://travel.home.sndimg.com/content/dam/images/travel/fullset/2014/07/20/32/food-paradise-102-ss-001.rend.hgtvcom.966.544.suffix/1491584380240.jpeg",
            "https://upload.wikimedia.org/wikipedia/commons/2/2e/Fast_food_meal.jpg",
            "https://www.cicis.com/media/1138/pizza_trad_pepperoni.png",
            "https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg",
            "http://www.seriouseats.com/recipes/assets_c/2017/02/20170216-detroit-style-pizza-43-thumb-1500xauto-436479.jpg",
            "http://dynl.mktgcdn.com/p/BqVXZELl1bpP82hnfZIUZJsRE3fh3cwTNK2-FvIqn5U/1538x776.png",
            "https://pbs.twimg.com/profile_images/739788751518453760/rXoHZ36l.jpg",
            "https://i.pinimg.com/originals/7d/03/ff/7d03ffe5b51269f02352b06502805429.jpg",
            "http://ichef.bbci.co.uk/news/624/cpsprodpb/16E81/production/_91452839_cafe-tazas.jpg",
            "https://estaticos.muyinteresante.es/media/cache/400x300_thumb/uploads/images/pyr/57d960705cafe802da8b4567/cafe.jpg"
    };

    private static String[] entImages = {
            "https://i2.wp.com/www.dondeir.com/wp-content/uploads/2017/06/beber-barato.jpg?fit=1024%2C767&ssl=1",
            "https://img.elcomercio.pe/files/ec_article_multimedia_gallery/uploads/2017/05/16/591aab13e45c4.jpeg",
            "https://www.absolutviajes.com/wp-content/uploads/2013/02/bares-San-Petersburgo.jpg",
            "https://i2.wp.com/www.eliberico.com/wp-content/uploads/2016/08/mejores-bares-de-londres.jpg?fit=640%2C426",
            "https://3.bp.blogspot.com/-WqnKaB7ZXR0/VGMMCZSKVWI/AAAAAAAAI8Y/OGSwq1zyOWw/s1600/mejores-bares-del-mundo.jpg",
            "http://d5qsyj6vaeh11.cloudfront.net/images/destinations/important%20places/irish%20pubs/article%20images/c112_i_main.jpg",
            "http://diariodegastronomia.com/wp-content/uploads/2016/01/Restaurantes-y-bares.jpg"
    };

    public static String getMockImage(String username) {
        String imageUrl = "";
        Random random = new Random();
        int index = 0;
        if (username.contains("comida")) {
            index = random.nextInt(foodImages.length);
            imageUrl = foodImages[index];
        } else if (username.contains("tecnologia")) {
            index = random.nextInt(techImages.length);
            imageUrl = techImages[index];
        } else if (username.contains("ropa")) {
            index = random.nextInt(clothesImages.length);
            imageUrl = clothesImages[index];
        } else {
            index = random.nextInt(entImages.length);
            imageUrl = entImages[index];
        }

        return imageUrl;
    }

}
