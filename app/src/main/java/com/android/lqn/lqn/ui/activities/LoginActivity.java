package com.android.lqn.lqn.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.model.User;
import com.android.lqn.lqn.restApi.ILoginResponse;
import com.android.lqn.lqn.ui.activities.consumer.UserHomeActivity;
import com.android.lqn.lqn.ui.activities.store.StoreHomeActivity;
import com.android.lqn.lqn.utils.ServiceConstants;


/**
 * Created by augustopinto on 5/16/17.
 */

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    private EditText mUsernameEditText;
    private EditText mPasswordEditText;
    private Button mLoginButton;
    private ProgressBar mLoadingProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        hideActionBar();
        initViews();
    }

    /**
     * Sets the action bar in order to set up the functionality and the design
     */
    private void hideActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    private void initViews() {
        mUsernameEditText = (EditText) findViewById(R.id.username);
        mPasswordEditText = (EditText) findViewById(R.id.password);
        mLoadingProgress = (ProgressBar) findViewById(R.id.loading_login);

        mLoginButton = (Button) findViewById(R.id.login_button);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateFields()) {
                    mLoadingProgress.setVisibility(View.VISIBLE);
                    mLoginButton.setVisibility(View.GONE);
                    Log.d(TAG, "Login in process.");

                    ILoginResponse loginResponse = new ILoginResponse() {
                        @Override
                        public void onSuccess(User user) {
                            mLoadingProgress.setVisibility(View.GONE);
                            mLoginButton.setVisibility(View.VISIBLE);
                            if (user.getUserType().getId() == ServiceConstants.NEW_USER) {
                                initFeedActivity();
                            } else if (user.getUserType().getId() == ServiceConstants.SHOP_USER) {
                                initShopActivity();
                            }
                        }

                        @Override
                        public void onFail(String errorMessage) {
                            mLoadingProgress.setVisibility(View.GONE);
                            mLoginButton.setVisibility(View.VISIBLE);
                            Toast.makeText(getApplicationContext(),
                                    errorMessage, Toast.LENGTH_LONG).show();
                        }
                    };

                    LoginController.getInstance(getApplicationContext())
                            .logIn(mUsernameEditText.getText().toString(),
                                    mPasswordEditText.getText().toString(),
                                    loginResponse);
                } else {
                    Log.e(TAG, "There is an error. Some field is empty");
                }
            }
        });
    }

    private boolean validateFields() {
        boolean isValid = true;
        if (TextUtils.isEmpty(mUsernameEditText.getText().toString())) {
            mUsernameEditText.setError(getString(R.string.default_validation_error));
            isValid = false;
        }
        if (TextUtils.isEmpty(mPasswordEditText.getText().toString())) {
            mPasswordEditText.setError(getString(R.string.default_validation_error));
            isValid = false;
        }
        return isValid;
    }

    private void initFeedActivity() {
        Intent intent = new Intent(getApplicationContext(), UserHomeActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * This method starts the screen to upload a new offer. Later this should change and
     * the method should starts the shop profile screen.
     */
    private void initShopActivity() {
        Intent intent = new Intent(getApplicationContext(), StoreHomeActivity.class);
        startActivity(intent);
        finish();
    }

}
