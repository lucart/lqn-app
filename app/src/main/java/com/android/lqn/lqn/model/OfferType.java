package com.android.lqn.lqn.model;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by augustopinto on 4/13/17.
 */

public class OfferType implements Serializable {

    @SerializedName(ServiceConstants.ID_ATTRIBUTE)
    private int mId;

    @SerializedName(ServiceConstants.NAME_ATTRIBUTE)
    private String mName;

    @SerializedName(ServiceConstants.ACTUAL_PRICE)
    private float mActualPrice;

    @SerializedName(ServiceConstants.PREVIOUS_PRICE)
    private float mPreviousPrice;

    @SerializedName(ServiceConstants.COMBO_TYPE_ATTRIBUTE_NAME)
    private String mComboType;

    @SerializedName(ServiceConstants.DISCOUNT_PERCENTAGE)
    private float mDiscountPercentage;

    public OfferType(int id, String name, float actualPrice, float previousPrice, String combotype,
                     float discountPercentage) {
        mId = id;
        mName = name;
        mActualPrice = actualPrice;
        mPreviousPrice = previousPrice;
        mComboType = combotype;
        mDiscountPercentage = discountPercentage;
    }

    public OfferType() {
        this(-1, "", 0, 0, "", 0);
    }

    public OfferType(JSONObject jsonObject) {
        parseToObject(jsonObject);
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public float getActualPrice() {
        return mActualPrice;
    }

    public void setActualPrice(float mActualPrice) {
        this.mActualPrice = mActualPrice;
    }

    public float getPreviousPrice() {
        return mPreviousPrice;
    }

    public void setPreviousPrice(float mPreviousPrice) {
        this.mPreviousPrice = mPreviousPrice;
    }

    public String getComboType() {
        return mComboType;
    }

    public void setComboType(String mComboType) {
        this.mComboType = mComboType;
    }

    public float getDiscountPercentage() {
        return mDiscountPercentage;
    }

    public void setDiscountPercentage(float mDiscountPercentage) {
        this.mDiscountPercentage = mDiscountPercentage;
    }

    public JSONObject parseToJson() {
        JSONObject json = new JSONObject();
        try {
            json.put(ServiceConstants.ID_ATTRIBUTE, mId);
            json.put(ServiceConstants.NAME_ATTRIBUTE, mName);
            json.put(ServiceConstants.ACTUAL_PRICE, mActualPrice);
            json.put(ServiceConstants.PREVIOUS_PRICE, mPreviousPrice);
            json.put(ServiceConstants.DISCOUNT_PERCENTAGE, mDiscountPercentage);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  json;
    }

    private void parseToObject(JSONObject json) {
        try {
            if (json.has(ServiceConstants.ID_ATTRIBUTE)) {
                mId = json.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.NAME_ATTRIBUTE)) {
                mName = json.getString(ServiceConstants.NAME_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.ACTUAL_PRICE)) {
                mActualPrice = (float) json.getDouble(ServiceConstants.ACTUAL_PRICE);
            }
            if (json.has(ServiceConstants.PREVIOUS_PRICE)) {
                mPreviousPrice = (float) json.getDouble(ServiceConstants.PREVIOUS_PRICE);
            }
            if (json.has(ServiceConstants.DISCOUNT_PERCENTAGE)) {
                mDiscountPercentage = (float) json.getDouble(ServiceConstants.DISCOUNT_PERCENTAGE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
