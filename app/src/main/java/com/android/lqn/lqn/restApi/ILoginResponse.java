package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.User;

/**
 * Created by augustopinto on 5/19/17.
 */

public interface ILoginResponse {

    void onSuccess(User user);

    void onFail(String errorMessage);
}
