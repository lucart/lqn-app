package com.android.lqn.lqn.ui.activities.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.model.ValidatedOffer;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class CheckoutActivity extends AppCompatActivity {

    private static final String TAG = CheckoutActivity.class.getSimpleName();

    private ValidatedOffer mValidatedOffer;
    private TextView mUsernameTextView;
    private TextView mUserLevelTextView;
    private ImageView mOfferImageView;
    private TextView mOfferDescriptionTextView;
    private TextView mOfferTypeTextView;
    private TextView mOfferLqnPointsTextView;
    private TextView mOfferRefundTextView;
    private EditText mOfferDiscountEntryEditText;
    private EditText mOfferExtraEntryEditText;
    private Button mCancelButton;
    private Button mConfirmButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        initActionBar();
        initValidatedOffer();
        initComponents();
    }

    private void initValidatedOffer() {
        String offer = "";
        if (getIntent().getExtras() != null) {
            offer = getIntent().getExtras().getString(ServiceConstants.INTENT_VALIDATED_OFFER_EXTRA);
        }
        if (!TextUtils.isEmpty(offer)) {
            try {
                mValidatedOffer = new ValidatedOffer(new JSONObject(offer));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void initComponents() {
        mUsernameTextView = (TextView) findViewById(R.id.username_data);
        mUserLevelTextView = (TextView) findViewById(R.id.username_level_data);
        mOfferImageView = (ImageView) findViewById(R.id.offer_image);
        mOfferDescriptionTextView = (TextView) findViewById(R.id.offer_title);
        mOfferTypeTextView = (TextView) findViewById(R.id.offer_type);
        mOfferLqnPointsTextView = (TextView) findViewById(R.id.lqn_points);
        mOfferRefundTextView = (TextView) findViewById(R.id.money_refund);
        mOfferDiscountEntryEditText = (EditText) findViewById(R.id.discount_entry);
        mOfferExtraEntryEditText = (EditText) findViewById(R.id.extra_purchase_entry);
        mCancelButton = (Button) findViewById(R.id.cancel_button);
        mConfirmButton = (Button) findViewById(R.id.confirm_button);
    }

    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.dashboard_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().show();

        }
    }

    /**
     * Method that fill the fields with the object data
     */
    private void showOffer() {
        if (mValidatedOffer != null) {
            // fill with the user data
            String userName = mValidatedOffer.getUser().getUsername();
            if (!TextUtils.isEmpty(userName)) {
                mUsernameTextView.setText(String.format(getString(R.string.checkout_username_data), userName));
            }
            //todo hide the level. Has to be implemented
            mUserLevelTextView.setVisibility(View.GONE);

            //Fill with offer data
            Picasso.with(getApplicationContext()).load(mValidatedOffer.getOffer().getImageUrl())
                    .into(mOfferImageView);

            if (!TextUtils.isEmpty(mValidatedOffer.getOffer().getName())) {
                mOfferDescriptionTextView.setText(mValidatedOffer.getOffer().getName());
            }

            setOfferType();
            setLqnPoints();
            setDiscountPercentage();

        } else {
            Log.d(TAG, "Validated offer is null");
        }
    }

    private void setOfferType() {
        switch (mValidatedOffer.getOffer().getOfferType().getId()) {
            case ServiceConstants.TWO_FOR_ONE_ID:
                String comboType = getResources().getStringArray(R.array.offer_types_dashboard)[ServiceConstants.TWO_FOR_ONE_ID];
                mOfferTypeTextView.setText(comboType);
                break;
            case ServiceConstants.DISCOUNT_ID:
                String discountType = getResources().getStringArray(R.array.offer_types_dashboard)[ServiceConstants.DISCOUNT_ID];
                mOfferTypeTextView.setText(discountType);
                break;
            case ServiceConstants.OLD_NEW_ID:
                String oldNewOffer = getResources().getStringArray(R.array.offer_types_dashboard)[ServiceConstants.OLD_NEW_ID];
                mOfferTypeTextView.setText(oldNewOffer);
                break;
            default:
                mOfferTypeTextView.setText("Unknown");
                break;
        }
    }

    private void setLqnPoints() {
        String lqnPoints = String.valueOf(mValidatedOffer.getOffer().getLqnPoints());
        mOfferLqnPointsTextView.setText(getString(R.string.score_dialog_lqn_points,
                lqnPoints), TextView.BufferType.SPANNABLE);

        Spannable spannable = (Spannable) mOfferLqnPointsTextView.getText();
        int spanLength = lqnPoints.length() + 1;
        int color = getApplicationContext().getResources().getColor(R.color.mainColorLight);
        spannable.setSpan(new ForegroundColorSpan(color), spannable.length() - spanLength,
                spannable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

    }

    private void setDiscountPercentage() {
        String refund = String.valueOf(mValidatedOffer.getOffer().getRefoundPercent());
        mOfferRefundTextView.setText(getString(R.string.score_dialog_discount, refund),
                TextView.BufferType.SPANNABLE);

        Spannable spannable = (Spannable) mOfferRefundTextView.getText();
        int spanLength = refund.length() + 1;
        int color = getApplicationContext().getResources().getColor(R.color.mainColorLight);
        spannable.setSpan(new ForegroundColorSpan(color), spannable.length() - spanLength -1,
                spannable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        initValidatedOffer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        showOffer();
    }
}
