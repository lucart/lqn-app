package com.android.lqn.lqn.ui.widgets;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.controller.StoreActionsController;
import com.android.lqn.lqn.model.ValidatedOffer;
import com.android.lqn.lqn.restApi.IValidatedOfferResponse;
import com.android.lqn.lqn.ui.activities.store.CheckoutActivity;
import com.android.lqn.lqn.utils.ServiceConstants;

import org.json.JSONException;

import java.util.Locale;

/**
 * This View represents the validator widget who appears throughout the store app.
 * Created by augustopinto on 11/13/17.
 */

public class ValidatorWidget extends RelativeLayout implements IValidatedOfferResponse{

    private Context mContext;
    private EditText mCodeEditText;
    private Button mValidateCodeButton;

    public ValidatorWidget(Context context) {
        this(context, null);
    }

    public ValidatorWidget(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ValidatorWidget(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initComponents();
    }

    private void initComponents() {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        addView(mInflater.inflate(R.layout.validator_widget, null));

        mCodeEditText = (EditText) findViewById(R.id.code_edit_text);
        mValidateCodeButton = (Button) findViewById(R.id.validate_button);
        mValidateCodeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String userId = "";
                try {
                    userId = String.valueOf(LoginController.getInstance(mContext).getUserInfo().getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                StoreActionsController.getInstance().findOfferByHashCode(mCodeEditText.getText().toString(), userId, ValidatorWidget.this);
            }
        });
    }

    private void goToCheckoutActivity(ValidatedOffer offer) {
        Intent intent = new Intent(mContext, CheckoutActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(ServiceConstants.INTENT_VALIDATED_OFFER_EXTRA, offer.parseToJSON().toString());
        mContext.startActivity(intent);
    }

    @Override
    public void onSuccess(ValidatedOffer validatedOffer) {
        mCodeEditText.setText("");
        goToCheckoutActivity(validatedOffer);
    }

    @Override
    public void onFail(String message) {
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }
}
