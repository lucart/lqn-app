package com.android.lqn.lqn.ui.activities.store;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.controller.StoreActionsController;
import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.model.Store;
import com.android.lqn.lqn.restApi.IDeleteOfferAction;
import com.android.lqn.lqn.restApi.IGetOfferListResponse;
import com.android.lqn.lqn.ui.activities.LoginActivity;
import com.android.lqn.lqn.ui.adapters.store.DashboardAdapter;
import com.android.lqn.lqn.utils.EndlessRecyclerViewScrollListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.List;

/**
 * Created by augustopinto on 7/18/17.
 */

public class StoreHomeActivity extends AppCompatActivity implements IGetOfferListResponse,
        NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = StoreHomeActivity.class.getSimpleName();

    private final int UPLOAD_OFFER_RESULT_CODE = 1;
    private TextView mHeaderText;
    private TextView mEmptyListMessage;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private DashboardAdapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private EndlessRecyclerViewScrollListener mScrollListener;
    private int mCurrentPage;
    private int mFirstPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.store_home);
        mFirstPage = getResources().getInteger(R.integer.first_page);
        mCurrentPage = mFirstPage;
        initActionBar();
        initNavigationDrawer();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initOfferList();
    }

    /**
     * Inits all the UI Views.
     */
    private void initViews() {
        mHeaderText = (TextView) findViewById(R.id.offers_published);
        mEmptyListMessage = (TextView) findViewById(R.id.empty_list_message);
        mProgressBar = (ProgressBar) findViewById(R.id.loading_progress_bar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                mScrollListener.resetState();
                mCurrentPage = mFirstPage;
                initOfferList();
            }
        });

        initRecyclerView();
    }

    /**
     * Inits the recycler view and its scroll listener in order to load elements dynamically
     */
    private void initRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.offers_list);
        mRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());

        mScrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mCurrentPage = ++ mCurrentPage;
                Log.d(TAG, "should load more items for page " + page);
                Log.d(TAG, "Getting offers for page " + mCurrentPage);
                Log.d(TAG, "the total items count is " + totalItemsCount);
                getOfferList();
            }
        };

        mAdapter = new DashboardAdapter(getApplicationContext());
        setAdaptersListeners();

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addOnScrollListener(mScrollListener);
    }

    /**
     * Inits the Action Bar displayed in the activity.
     */
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(getString(R.string.dashboard_title));
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().show();
        }
    }

    /**
     * Inits the activity Navigation Drawer. This Navigation drawer will show an option menu
     * when the user swipes the screen from left to right.
     */
    private void initNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.open_nav_drawer, R.string.close_nav_drawer);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        NavigationView navView = (NavigationView) findViewById(R.id.navigation_view);
        initProfileInfo(navView);
        navView.setNavigationItemSelectedListener(this);
    }

    /**
     * This method fill the navigation view that is sent by param with the user profile info.
     * @param navView
     */
    private void initProfileInfo(NavigationView navView) {
        TextView storeName = (TextView) navView.getHeaderView(0).findViewById(R.id.nav_header_username);
        ImageView storeImage = (ImageView) navView.getHeaderView(0).findViewById(R.id.nav_header_profile_picture);
        try {
            Store store = LoginController.getInstance(getApplicationContext())
                    .getUserInfo().getStore();
            storeName.setText(store.getName());

            Picasso.with(getApplicationContext())
                    .load(store.getImageUrl()).into(storeImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Init the offer list that will be shown to the user.
     */
    private void initOfferList() {
        if (mCurrentPage == mFirstPage) {
            mProgressBar.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.INVISIBLE);
        }
        getOfferList();
    }

    /**
     * Makes the API call and receive the offer list from the current page.
     */
    private void getOfferList() {
        int id = -1;
        try {
            id = LoginController.getInstance(
                    getApplicationContext()).getUserInfo().getStore().getId();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        StoreActionsController.getInstance().getAllOffers(mCurrentPage, id, this);
    }

    /**
     * This method starts an intent going to UploadNewOfferActivity
     * @param isEditMode
     * @param offer
     */
    private void initNewOfferActivity(boolean isEditMode, Offer offer) {
        Intent intent = new Intent(this, UploadNewOfferActivity.class);
        intent.putExtra(UploadNewOfferActivity.IS_EDIT_MODE, isEditMode);
        if (isEditMode && offer != null) {
            intent.putExtra(UploadNewOfferActivity.OFFER, offer.parseToJSON().toString());
        }
        startActivityForResult(intent, UPLOAD_OFFER_RESULT_CODE);
    }

    /**
     * Logs the user out.
     */
    private void logout() {
        LoginController.getInstance(getApplicationContext()).logout();
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Set some important listeners to the adapter.
     *  - DeleteIconClickListener will be executed when the user press the delete button in the list
     *  - EditButtonClickListener will be executed when the user press the edit button in the list
     */
    private void setAdaptersListeners() {
        mAdapter.setDeleteIconClickListener(new DashboardAdapter.onDeleteOfferListener() {
            @Override
            public void onItemDeleted(final int position) {
                StoreActionsController.getInstance().deleteOffer(mAdapter.getOfferList().get(position).getId(),
                        new IDeleteOfferAction() {
                            @Override
                            public void onSuccess() {
                                mHeaderText.setText(String.format(getString(R.string.offers_published), mAdapter.getOfferList().size()));
                                mAdapter.notifyItemRemoved(position);
                            }

                            @Override
                            public void onFail(String message) {
                                Toast.makeText(getApplicationContext(),
                                        "Error al borrar la oferta",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });

        mAdapter.setEditButtonClickListener(new DashboardAdapter.onEditOfferListener() {
            @Override
            public void onItemSelected(int position) {
                initNewOfferActivity(true, mAdapter.getOfferList().get(position));
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onSuccess(final List<Offer> offerList) {
        Log.d(TAG, "Offers retrieved. Size " + offerList.size());
        mProgressBar.setVisibility(View.INVISIBLE);
        if (mCurrentPage > mFirstPage) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mAdapter.updateOfferList(offerList);
            mHeaderText.setText(String.format(getString(R.string.offers_published), mAdapter.getItemCount()));
        } else {
            if (!offerList.isEmpty()) {
                mAdapter.setOfferList(offerList);
                mEmptyListMessage.setVisibility(View.INVISIBLE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mHeaderText.setVisibility(View.VISIBLE);
                mHeaderText.setText(String.format(getString(R.string.offers_published), offerList.size()));
            } else {
                mHeaderText.setVisibility(View.INVISIBLE);
                mRecyclerView.setVisibility(View.INVISIBLE);
                mEmptyListMessage.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onFail(String message) {
        if (mCurrentPage == mFirstPage) {
            mHeaderText.setVisibility(View.INVISIBLE);
            mProgressBar.setVisibility(View.INVISIBLE);
            mRecyclerView.setVisibility(View.INVISIBLE);
            mEmptyListMessage.setVisibility(View.VISIBLE);
        }
        Log.e(TAG, message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.store_dashboard_action_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_add_offer:
                initNewOfferActivity(false, null);
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == UPLOAD_OFFER_RESULT_CODE) {
            initOfferList();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_logout:
                logout();
                break;
        }
        return false;
    }
}