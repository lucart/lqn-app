package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.Offer;

import java.util.List;

/**
 * Created by augustopinto on 4/4/17.
 */

public interface IOffersLoaded {

    void onSuccess(List<Offer> offers);

    void onFail();
}
