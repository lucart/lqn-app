package com.android.lqn.lqn.model;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by augustopinto on 4/13/17.
 */

public class Store implements Serializable {

    @SerializedName(ServiceConstants.ID_ATTRIBUTE)
    private int mId;

    @SerializedName(ServiceConstants.NAME_ATTRIBUTE)
    private String mName;

    @SerializedName(ServiceConstants.DESCRIPTION_ATTRIBUTE)
    private String mDescription;

    @SerializedName(ServiceConstants.IMAGE_URL_ATTRIBUTE)
    private String mImageUrl;

    public Store(int id, String name, String desc, String imageUrl) {
        mId = id;
        mName = name;
        mDescription = desc;
        mImageUrl = imageUrl;
    }

    public Store() {
        this(-1, "", "", "");
    }

    public Store(JSONObject store) {
        parseToObject(store);
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public JSONObject parseToJson() {
        JSONObject json = new JSONObject();
        try {
            json.put(ServiceConstants.ID_ATTRIBUTE, mId);
            json.put(ServiceConstants.NAME_ATTRIBUTE, mName);
            json.put(ServiceConstants.DESCRIPTION_ATTRIBUTE, mDescription);
            json.put(ServiceConstants.IMAGE_URL_ATTRIBUTE, mImageUrl);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    private void parseToObject(JSONObject store) {
        try {
            if (store.has(ServiceConstants.ID_ATTRIBUTE)) {
                mId = store.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
            if (store.has(ServiceConstants.NAME_ATTRIBUTE)) {
                mName = store.getString(ServiceConstants.NAME_ATTRIBUTE);
            }
            if (store.has(ServiceConstants.DESCRIPTION_ATTRIBUTE)) {
                mDescription = store.getString(ServiceConstants.DESCRIPTION_ATTRIBUTE);
            }
            if (store.has(ServiceConstants.IMAGE_URL_ATTRIBUTE)) {
                mImageUrl = store.getString(ServiceConstants.IMAGE_URL_ATTRIBUTE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
