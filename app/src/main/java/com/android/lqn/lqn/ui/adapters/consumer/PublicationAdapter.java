package com.android.lqn.lqn.ui.adapters.consumer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.ui.activities.consumer.UserHomeActivity;
import com.android.lqn.lqn.ui.dialogs.LqnScoreDialog;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by augustopinto on 3/24/17.
 */

public class PublicationAdapter extends RecyclerView.Adapter<PublicationAdapter.ViewHolder> {

    public enum OfferType {
        OFFER_FEED,
        OFFER_ACTIVE_OFFER
    }

    private List<Offer> mOffers;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private View mView;
        public TextView mTitleTextView;
        public TextView mShopTextView;
        public ImageView mImageImageView;
        public ImageView mPlaceHolderImageView;
        public TextView mOfferTypeTextView;
        public Button mActionButton;


        public ViewHolder(View view) {
            super(view);
            mView = view;
            initViews();
        }

        private void initViews() {
            mTitleTextView = (TextView) mView.findViewById(R.id.publication_title);
            mShopTextView = (TextView) mView.findViewById(R.id.publication_shop);
            mImageImageView = (ImageView) mView.findViewById(R.id.publication_image);
            mPlaceHolderImageView = (ImageView) mView.findViewById(R.id.placeholder_image);
            mOfferTypeTextView = (TextView) mView.findViewById(R.id.offer_type_label);
            mActionButton = (Button) mView.findViewById(R.id.action_button);
        }

    }

    private Context mContext;
    private UserHomeActivity.OnWantItButtonClickListener mListener;
    private LqnScoreDialog mLqnDialog;
    private OfferType mType;

    public PublicationAdapter(Context context, List<Offer> offers, LqnScoreDialog dialog, OfferType type) {
        mContext = context;
        mLqnDialog = dialog;
        mType = type;

        if (offers == null) {
            mOffers = new ArrayList<>();
        } else {
            mOffers = offers;
        }
    }

    public PublicationAdapter(Context context, List<Offer> offers, OfferType type) {
        mContext = context;
        mType = type;

        if (offers == null) {
            mOffers = new ArrayList<>();
        } else {
            mOffers = offers;
        }
    }

    public void updateOfferList(List<Offer> newOffers) {
        mOffers.addAll(newOffers);
        notifyDataSetChanged();
    }

    public void setOfferList(List<Offer> newOffers) {
        mOffers = newOffers;
        notifyDataSetChanged();
    }

    public void setWantItButtonClickListener(UserHomeActivity.OnWantItButtonClickListener onClickListener) {
        mListener = onClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.offer_item, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        //Fill the view with the data.
        final Offer offer = mOffers.get(position);

        holder.mTitleTextView.setText(offer.getName());
        holder.mShopTextView.setText(offer.getShop().getName());
        setOfferTypeLabel(offer, holder.mOfferTypeTextView);
        if (!TextUtils.isEmpty(offer.getImageUrl())) {
            Picasso.with(mContext).load(offer.getImageUrl())
                    .into(holder.mImageImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.mPlaceHolderImageView.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } else {
            holder.mImageImageView.setImageResource(android.R.color.transparent);
            holder.mPlaceHolderImageView.setVisibility(View.VISIBLE);
        }

        holder.mImageImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLqnDialog != null) {
                    mLqnDialog.showDialog(offer);
                }
            }
        });

        if (mType == OfferType.OFFER_ACTIVE_OFFER) {
            holder.mActionButton.setBackgroundColor(mContext.getResources().getColor(R.color.offerTypeButton));
            String hashCode = "Cod: " + offer.getHashCode();
            holder.mActionButton.setText(hashCode);

        } else if (mType == OfferType.OFFER_FEED){
            holder.mActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onWantItClicked(offer.getId());
                    }
                }
            });
        }
    }

    private void setOfferTypeLabel(Offer offer, TextView tv) {
        String label = "";
        switch (offer.getOfferType().getId()) {
            case ServiceConstants.TWO_FOR_ONE_ID:
                label = "2x1";
                break;
            case ServiceConstants.OLD_NEW_ID:
                String oldP = "$" + String.valueOf(offer.getOfferType().getPreviousPrice());
                String newP = " $" + offer.getOfferType().getActualPrice();
                label = oldP + newP;
                break;
            case ServiceConstants.DISCOUNT_ID:
                label = "% -" + offer.getOfferType().getDiscountPercentage();
                break;

        }
        tv.setText(label, TextView.BufferType.SPANNABLE);
        if (offer.getOfferType().getId() == ServiceConstants.OLD_NEW_ID) {
            Spannable spannable = (Spannable) tv.getText();
            int endSpan = String.valueOf(offer.getOfferType().getPreviousPrice()).length() + 1;
            int color = mContext.getResources().getColor(R.color.mediumGray);
            spannable.setSpan(new StrikethroughSpan(), 0, endSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannable.setSpan(new ForegroundColorSpan(color), 0, endSpan, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    @Override
    public int getItemCount() {
        return mOffers.size();
    }
}
