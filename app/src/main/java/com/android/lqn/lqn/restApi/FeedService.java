package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.JsonObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by augustopinto on 4/4/17.
 */

public interface FeedService {

    @GET(ServiceConstants.GET_OFFERS)
    Call<List<Offer>> getOffers(@Path("page") int page, @Path("offerTypeId") int offerTypeId);

    @GET(ServiceConstants.RESERVE_OFFER)
    Call<JsonObject> reserveOffer(@Path("offerId") int offerId, @Path("userId") int userId);

    @GET(ServiceConstants.GET_OFFERS_BY_USER_ID)
    Call<List<Offer>> getOffersByUser(@Path("page") int page, @Path("userId") int userId);

}
