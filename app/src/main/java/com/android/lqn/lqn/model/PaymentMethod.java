package com.android.lqn.lqn.model;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by augustopinto on 4/13/17.
 */

public class PaymentMethod implements Serializable {

    @SerializedName(ServiceConstants.ID_ATTRIBUTE)
    private int mId;

    @SerializedName(ServiceConstants.NAME_ATTRIBUTE)
    private String mName;

    public PaymentMethod(int id, String name) {
        mId = id;
        mName = name;
    }

    public PaymentMethod() {
        this(-1, "");
    }

    public PaymentMethod(JSONObject json) {
        parseToObject(json);
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public JSONObject parseToJson() {
        JSONObject json = new JSONObject();
        try {
            json.put(ServiceConstants.ID_ATTRIBUTE, mId);
            json.put(ServiceConstants.NAME_ATTRIBUTE, mName);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    private void parseToObject(JSONObject json) {
        try {
            if (json.has(ServiceConstants.ID_ATTRIBUTE)) {
                mId = json.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.NAME_ATTRIBUTE)) {
                mName = json.getString(ServiceConstants.NAME_ATTRIBUTE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
