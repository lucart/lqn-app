package com.android.lqn.lqn.controller;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Debug;
import android.text.TextUtils;
import android.util.Log;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.model.User;
import com.android.lqn.lqn.restApi.ILoginResponse;
import com.android.lqn.lqn.restApi.LoginService;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * This controller should be responsible for the following actions:
 *      - Log in a user into the system.
 *      - Log out a user
 * Created by augustopinto on 5/16/17.
 */

public class LoginController {

    private static final String TAG = LoginController.class.getSimpleName();

    private static LoginController mInstance;

    private Retrofit mRestApiCore;

    private Context mContext;

    private LoginController(Context context) {
        mContext = context;
        initApiRestCore();
    }

    public static LoginController getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new LoginController(context);
        }
        return mInstance;
    }

    private void initApiRestCore() {
        Gson gson = new GsonBuilder().create();

        mRestApiCore = new Retrofit.Builder().baseUrl(ServiceConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /**
     * Checks if there is any user logged in.
     * @return true if there is an user logged in. False otherwise.
     */
    public boolean isLogged() {
        SharedPreferences preferences = mContext.getSharedPreferences(
                mContext.getString(R.string.preferences_file), Context.MODE_PRIVATE);
        String username = preferences.getString(ServiceConstants.USER_INFO, "");

        return !TextUtils.isEmpty(username);

    }

    public void logIn(final String username, String password, final ILoginResponse loginResponse) {

        LoginService loginService = mRestApiCore.create(LoginService.class);

        Call<User> call = loginService.login(username, password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Log.i(TAG, "Response code: " + response.code());
                if (response.code() == ServiceConstants.NO_ERROR) {
                    Log.i(TAG, "Call " + call.toString());
                    Log.i(TAG, "Response message " + response.code());
                    User user = response.body();
                    saveUserData(user);
                    loginResponse.onSuccess(response.body());
                } else {
                    Log.e(TAG, response.message());

                    loginResponse.onFail(response.message());
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    /**
     * Saves the user json object in order to use it over the whole app.
     * @param user the object to be saved
     */
    private void saveUserData(User user) {
        SharedPreferences preferences = mContext.getSharedPreferences(
                mContext.getString(R.string.preferences_file), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ServiceConstants.USER_INFO, user.parseToJson().toString());
        editor.commit();
    }

    public void logout() {
        SharedPreferences preferences = mContext.getSharedPreferences(
                mContext.getString(R.string.preferences_file), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(ServiceConstants.USER_INFO);
        editor.commit();
    }

    /**
     * gets all the user info that is logged in.
     * For now the method only returns the username
     * @return the username.
     */
    public User getUserInfo() throws JSONException {
        SharedPreferences preferences = mContext.getSharedPreferences(
                mContext.getString(R.string.preferences_file), Context.MODE_PRIVATE);
        User user;
        JSONObject json = new JSONObject(preferences.getString(ServiceConstants.USER_INFO, ""));
        user = new User(json);
        return user;
    }

    @Deprecated
    public int getUserType() {
        SharedPreferences preferences = mContext.getSharedPreferences(
                mContext.getString(R.string.preferences_file), Context.MODE_PRIVATE);
        return preferences.getInt(ServiceConstants.USER_TYPE_ATTRIBUTE, -1);
    }

}
