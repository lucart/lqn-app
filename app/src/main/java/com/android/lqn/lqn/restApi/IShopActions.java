package com.android.lqn.lqn.restApi;

/**
 * Created by augustopinto on 6/29/17.
 */

public interface IShopActions {

    void onSuccess();
    void onFail(String errorMessage);

}
