package com.android.lqn.lqn.utils;

/**
 * Created by augustopinto on 4/4/17.
 */

public class ServiceConstants {

    public static String BASE_URL = "http://lqn-env.tg2hn4muck.us-east-1.elasticbeanstalk.com/";

    // Response codes
    public static final int UNAUTHORIZED = 401;
    public static final int NO_ERROR = 200;

    // Endpoints
    public static final String GET_OFFERS = "/getOffers/{page}/{offerTypeId}";
    public static final String LOGIN = "/login/{username}/{password}";
    public static final String UPLOAD_OFFERS = "/insertOffer/";
    public static final String GET_OFFER_BY_ID = "/getOffersByShop/{page}/{storeId}";
    public static final String DELETE_OFFER = "/deleteOffer/{offerId}";
    public static final String UPDATE_OFFER = "/updateOffer/";
    public static final String RESERVE_OFFER = "/reserveOffer/{offerId}/{userId}";
    public static final String FIND_OFFER_BY_HASH = "/findOfferByHashCode/{hashCode}/{userId}";
    public static final String GET_OFFERS_BY_USER_ID = "/getOffersByUser/{page}/{userId}";

    // Attributes
    public static final String ID_ATTRIBUTE = "id";
    public static final String NAME_ATTRIBUTE = "name";
    public static final String DESCRIPTION_ATTRIBUTE = "description";
    public static final String EXPIRATION_DATE_ATTRIBUTE = "expirationDate";
    public static final String STOCK_ATTRIBUTE = "stock";
    public static final String SHOP_ATTRIBUTE = "shop";
    public static final String SHOP_ID_ATTRIBUTE = "shopId";
    public static final String OFFER_TYPE_ATTRIBUTE = "offerType";
    public static final String OFFER_TYPE_ID_ATTRIBUTE = "offerTypeId";
    public static final String PAYMENT_METHOD_ATTRIBUTE = "paymentMethod";
    public static final String PAYMENT_METHOD_ID_ATTRIBUTE = "paymentMethodId";
    public static final String REFOUND_PERCENT_ATTRIBUTE = "refundMoneyPercentage";
    public static final String DISCOUNT_PERCENTAGE = "discountPercentage";
    public static final String IMAGE_URL_ATTRIBUTE = "imageUrl";
    public static final String LQN_SCORE= "lqnScore";
    public static final String OFFER_VALIDATED_ATTRIBUTE = "offer";
    public static final String USER_VALIDATED_ATTRIBUTE = "user";

    // Offer object attributes
    public static final String LQN_POINTS = "lqnLoyaltyPoints";
    public static final String AMOUNT_RESERVED = "amountReserv";
    public static final String HASH_CODE = "hashCode";

    // OfferType object attributes
    public static final String ACTUAL_PRICE = "actualPrice";
    public static final String PREVIOUS_PRICE = "previousPrice";
    public static final String COMBO_TYPE_ATTRIBUTE_NAME = "comboType";

    // User object attributes
    public static final String USER_INFO = "user_object";
    public static final String USERNAME_ATTRIBUTE = "username";
    public static final String PASSWORD_ATTRIBUTE = "password";
    public static final String USER_TYPE_ATTRIBUTE = "userType";

    // Store object attributes
    public static final String STORE_OBJ_ATTRIBUTE = "store";

    //UserTypes
    public static final int NEW_USER = 1;
    public static final int SHOP_USER = 2;

    // OfferTypes
    public static final int OLD_NEW_ID = 1;
    public static final int TWO_FOR_ONE_ID = 2;
    public static final int DISCOUNT_ID = 3;

    // Payment method
    public static final int CASH = 1;
    public static final int CARDS = 2;

    // Intent extras
    public static final String INTENT_VALIDATED_OFFER_EXTRA = "validatedOffer";

}
