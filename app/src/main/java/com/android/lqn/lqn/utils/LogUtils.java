package com.android.lqn.lqn.utils;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by augustopinto on 11/8/17.
 */

public class LogUtils {

    public static void logResponseMessage(String tag, Call call, Response response) {
        Log.i(tag, "Response code: " + response.code());
        Log.i(tag, "Call: " + call.request().toString());
        Log.i(tag, "Response message: " + response.message());
    }

}
