package com.android.lqn.lqn.ui.activities.store;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.controller.StoreActionsController;
import com.android.lqn.lqn.restApi.IShopActions;
import com.android.lqn.lqn.utils.BasicUtils;
import com.android.lqn.lqn.utils.ImageBattery;
import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by augustopinto on 5/24/17.
 */

public class UploadNewOfferActivity extends AppCompatActivity implements IShopActions {
    private final String TAG = UploadNewOfferActivity.class.getSimpleName();

    public static final String IS_EDIT_MODE = "isEditMode";
    public static final String OFFER = "offer";

    private final int UPLOAD_OFFER_RESULT_CODE = 1;
    private EditText mOfferTitle;
    private EditText mOfferDescription;
    private EditText mOfferStock;
    private EditText mPrevPrice;
    private EditText mNewPrice;
    private Spinner mRefundSpinner;
    private Spinner mLqnPointsSpinner;
    private Spinner mEndsSpinner;
    private Spinner mOfferTypeSpinner;
    private Spinner mDiscountPercentageSpinner;
    private CheckBox mCardsPaymentMethod;
    private LinearLayout mOldNewPriceContainer;
    private LinearLayout mDiscountCustomContainer;
    private Button mUploadButton;
    private FloatingActionButton mCancelButton;

    private int mOfferTypeId;

    //This int is only valid when the offer is being edited and is necessarily to update it.
    private int mOfferId;
    private boolean isEditMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_new_offer_activity);
        initViews();
        isEditMode = getIntent().getExtras().getBoolean(IS_EDIT_MODE);
        if (isEditMode) {
            Log.i(TAG, "Is in Edit mode");
            try {
                JSONObject offer = new JSONObject(getIntent().getStringExtra(OFFER));
                initEditModeViews(offer);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        hideActionBar();
    }

    private void hideActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }
    }

    private void initViews() {
        mOfferTitle = (EditText) findViewById(R.id.offer_title);
        mOfferDescription = (EditText) findViewById(R.id.offer_description);
        mOfferStock = (EditText) findViewById(R.id.stock_entry);
        mPrevPrice = (EditText) findViewById(R.id.old_price_entry);
        mNewPrice = (EditText) findViewById(R.id.new_price_entry);
        mOldNewPriceContainer = (LinearLayout) findViewById(R.id.old_new_container);
        mDiscountCustomContainer = (LinearLayout) findViewById(R.id.discount_custom_container);
        mCardsPaymentMethod = (CheckBox) findViewById(R.id.pay_met_cards_rb);

        initCancelButton();
        initUploadButton();
        initRefoundSpinner();
        initOfferTypeSpinner();
        initLqnPointsSpinner();
        initEndsSpinner();
        initDiscountSpinner();
    }

    private void initOfferTypeSpinner() {
        mOfferTypeSpinner = (Spinner) findViewById(R.id.offer_type);
        ArrayAdapter<CharSequence> adapter = getOfferTypesSpinnerAdapter();

        adapter.setDropDownViewResource(R.layout.spinner_item);
        mOfferTypeSpinner.setAdapter(adapter);
        mOfferTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 1) { //offer type is old > new
                    mOldNewPriceContainer.setVisibility(View.VISIBLE);
                    mDiscountCustomContainer.setVisibility(View.GONE);
                    mOfferTypeId = ServiceConstants.OLD_NEW_ID;
                } else if (i == 2) { //offer type is combo
                    mOldNewPriceContainer.setVisibility(View.GONE);
                    mDiscountCustomContainer.setVisibility(View.GONE);
                    mOfferTypeId = ServiceConstants.TWO_FOR_ONE_ID;
                } else if (i == 3){ //offer type is discount
                    mOldNewPriceContainer.setVisibility(View.GONE);
                    mDiscountCustomContainer.setVisibility(View.VISIBLE);
                    mOfferTypeId = ServiceConstants.DISCOUNT_ID;
                } else {
                    TextView tv = (TextView) view.findViewById(R.id.title);
                    tv.setTextColor(Color.GRAY);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    /**
     * Create an array with the offer types options.
     * @return the ArrayAdapter of OfferTypes.
     */
    private ArrayAdapter<CharSequence> getOfferTypesSpinnerAdapter() {
        CharSequence[] offerTypes = getResources().getTextArray(R.array.offer_types_dashboard);
        return new ArrayAdapter<CharSequence>(getApplicationContext(),
                R.layout.spinner_default_item, offerTypes) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                TextView tv = (TextView) view;
                if(position == 0){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
    }

    private void initLqnPointsSpinner() {
        mLqnPointsSpinner = (Spinner) findViewById(R.id.lqn_points_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.points, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mLqnPointsSpinner.setAdapter(adapter);
    }

    private void initEndsSpinner() {
        mEndsSpinner = (Spinner) findViewById(R.id.end_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.ends, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        mEndsSpinner.setAdapter(adapter);
    }

    private void initDiscountSpinner() {
        mDiscountPercentageSpinner = (Spinner) findViewById(R.id.discount_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.points, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mDiscountPercentageSpinner.setAdapter(adapter);
    }

    private void initRefoundSpinner() {
        mRefundSpinner = (Spinner) findViewById(R.id.refund_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(),
                R.array.refund_money_percentage, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        mRefundSpinner.setAdapter(adapter);
    }

    private void initUploadButton() {
        mUploadButton = (Button) findViewById(R.id.upload_offer);
        mUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateField()) {
                    mUploadButton.setBackgroundColor(
                            UploadNewOfferActivity.this.getResources().getColor(android.R.color.darker_gray));

                    mUploadButton.setClickable(false);
                    if (isEditMode) {
                        StoreActionsController.getInstance().updateOffer(mapOffer(), UploadNewOfferActivity.this);
                    } else {
                        StoreActionsController.getInstance().uploadNewOffer(mapOffer(), UploadNewOfferActivity.this);
                    }
                } else {
                    Toast.makeText(UploadNewOfferActivity.this,
                            getString(R.string.check_fields_default_error), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void initCancelButton() {
        mCancelButton = (FloatingActionButton) findViewById(R.id.cancel_button);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private boolean validateField() {
        boolean isValid = true;
        if (TextUtils.isEmpty(mOfferTitle.getText().toString())) {
            mOfferTitle.setError(getString(R.string.default_validation_error));
            isValid = false;
        }
        if (isValid && TextUtils.isEmpty(mOfferStock.getText().toString())) {
            mOfferStock.setError(getString(R.string.offer_stock_validation_error));
            isValid = false;
        }

        if (isValid && mOfferTypeSpinner.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
            isValid = false;
        } else if (isValid && mOfferTypeSpinner.getSelectedItemPosition() == ServiceConstants.OLD_NEW_ID) {
            if (TextUtils.isEmpty(mPrevPrice.getText().toString()) ||
                    TextUtils.isEmpty(mNewPrice.getText().toString())) {
                isValid = false;
            }
        } else if (isValid && mOfferTypeSpinner.getSelectedItemPosition() == ServiceConstants.DISCOUNT_ID) {
            if (mDiscountPercentageSpinner.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
                isValid = false;
            }
        }

        if (isValid && mEndsSpinner.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
            isValid = false;
        }

        if (isValid && mLqnPointsSpinner.getSelectedItemPosition() == Spinner.INVALID_POSITION) {
            isValid = false;
        }
        return isValid;
    }

    private int getPaymentMethodId() {
        int id = ServiceConstants.CASH;

        if (mCardsPaymentMethod.isChecked()) {
            id = ServiceConstants.CARDS;
        }
        return id;
    }

    private JsonObject mapOffer() {
        JsonObject json = new JsonObject();
        String offerName = mOfferTitle.getText().toString();
        json.addProperty(ServiceConstants.NAME_ATTRIBUTE, offerName);

        String offerDesc = mOfferDescription.getText().toString();
        json.addProperty(ServiceConstants.DESCRIPTION_ATTRIBUTE, offerDesc);

        int stock = Integer.parseInt(mOfferStock.getText().toString());
        json.addProperty(ServiceConstants.STOCK_ATTRIBUTE, stock);

        Calendar date = Calendar.getInstance();

        switch (mEndsSpinner.getSelectedItemPosition()) {
            case 0:
                date.add(Calendar.DATE, 3);
                break;
            case 1:
                date.add(Calendar.DATE, 7);
                break;
            case 2:
                date.add(Calendar.DATE, 10);
                break;
            case 3:
                date.add(Calendar.DATE, 14);
            default:
                break;
        }
        Log.i(TAG, "Date in epoch time " + date.getTimeInMillis()/1000);

        json.addProperty(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE, getExpirationDateEpoch(date));

        int shopId = -1;
        try {
            shopId = LoginController.getInstance(
                    getApplicationContext()).getUserInfo().getStore().getId();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        json.addProperty(ServiceConstants.SHOP_ID_ATTRIBUTE, shopId);
        json.addProperty(ServiceConstants.OFFER_TYPE_ID_ATTRIBUTE, mOfferTypeId);
        json.addProperty(ServiceConstants.PAYMENT_METHOD_ID_ATTRIBUTE, getPaymentMethodId());
        switch (mOfferTypeSpinner.getSelectedItemPosition()) {
            case ServiceConstants.OLD_NEW_ID:
                json.addProperty(ServiceConstants.ACTUAL_PRICE, mNewPrice.getText().toString());
                json.addProperty(ServiceConstants.PREVIOUS_PRICE, mPrevPrice.getText().toString());
                break;
            case ServiceConstants.DISCOUNT_ID:
                json.addProperty(ServiceConstants.DISCOUNT_PERCENTAGE, Integer.parseInt(mDiscountPercentageSpinner.getSelectedItem().toString()));
                break;
        }

        json.addProperty(ServiceConstants.LQN_POINTS, Integer.parseInt(mLqnPointsSpinner.getSelectedItem().toString()));
        json.addProperty(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE, Integer.parseInt(mRefundSpinner.getSelectedItem().toString()));

        if (isEditMode) {
            json.addProperty(ServiceConstants.ID_ATTRIBUTE, mOfferId);
        } else {
            //todo only for debug
            try {
                String username = LoginController.getInstance(getApplicationContext()).getUserInfo().getUsername();
                json.addProperty(ServiceConstants.IMAGE_URL_ATTRIBUTE, ImageBattery.getMockImage(username));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        // The offer is activated by default
        json.addProperty("enabled", true);
        Log.d(TAG, "offers json to be send: \n" + json.toString());
        return  json;
    }

    private void initEditModeViews(JSONObject offer) {
        try {
            mOfferTitle.setText(offer.getString(ServiceConstants.NAME_ATTRIBUTE));
            if (offer.has(ServiceConstants.DESCRIPTION_ATTRIBUTE)) {
                mOfferDescription.setText(offer.getString(ServiceConstants.DESCRIPTION_ATTRIBUTE));
            }

            if (offer.has(ServiceConstants.STOCK_ATTRIBUTE)) {
                mOfferStock.setText(offer.getString(ServiceConstants.STOCK_ATTRIBUTE));
            }

            JSONObject offerType = offer.getJSONObject(ServiceConstants.OFFER_TYPE_ATTRIBUTE);
            if (offerType.has(ServiceConstants.ID_ATTRIBUTE)) {
                int id = (int) offerType.get(ServiceConstants.ID_ATTRIBUTE);
                if (id == ServiceConstants.OLD_NEW_ID) {
                    mOfferTypeSpinner.setSelection(ServiceConstants.OLD_NEW_ID);
                    mPrevPrice.setText(String.valueOf(offerType.getInt(ServiceConstants.PREVIOUS_PRICE)));
                    mNewPrice.setText(String.valueOf(offerType.getInt(ServiceConstants.ACTUAL_PRICE)));
                } else if (id == ServiceConstants.TWO_FOR_ONE_ID) {
                    mOfferTypeSpinner.setSelection(ServiceConstants.TWO_FOR_ONE_ID);
                } else if (id == ServiceConstants.DISCOUNT_ID) {
                    mOfferTypeSpinner.setSelection(ServiceConstants.DISCOUNT_ID);

                    int as = offerType.getInt(ServiceConstants.DISCOUNT_PERCENTAGE);
                    int ref = 0;
                    for (int i = 0; i < getResources().getStringArray(R.array.points).length; i++) {
                        if (Integer.parseInt(mDiscountPercentageSpinner.getItemAtPosition(i).toString()) == as) {
                            ref = i;
                        }
                    }
                    mDiscountPercentageSpinner.setSelection(ref);
                }
            }

            if (offer.has(ServiceConstants.LQN_POINTS)) {
                int as = offer.getInt(ServiceConstants.LQN_POINTS);
                int ref = 0;
                for (int i = 0; i < getResources().getStringArray(R.array.points).length; i++) {
                    if (Integer.parseInt(mLqnPointsSpinner.getItemAtPosition(i).toString()) == as) {
                        ref = i;
                    }
                }
                mLqnPointsSpinner.setSelection(ref);
            }
            if (offer.has(ServiceConstants.PAYMENT_METHOD_ATTRIBUTE)) {
                JSONObject paymentMethod = offer.getJSONObject(ServiceConstants.PAYMENT_METHOD_ATTRIBUTE);
                int id = paymentMethod.getInt(ServiceConstants.ID_ATTRIBUTE);
                if (id == 2) {
                    mCardsPaymentMethod.setChecked(true);
                }
            }
            if (offer.has(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE)) {
                int as = offer.getInt(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE);
                int ref = 0;
                for (int i = 0; i < getResources().getStringArray(R.array.refund_money_percentage).length; i++) {
                    if (Integer.parseInt(mRefundSpinner.getItemAtPosition(i).toString()) == as) {
                        ref = i;
                    }
                }
                mRefundSpinner.setSelection(ref);
            }

            if (offer.has(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE)) {
                int daysLeft = BasicUtils.getExpirationDaysLeft(
                        offer.getLong(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE));
                if ((daysLeft < 2)) {
                    mEndsSpinner.setSelection(0);
                } else if (daysLeft > 2 && daysLeft < 8) {
                    mEndsSpinner.setSelection(1);
                } else if (daysLeft > 7 && daysLeft < 15) {
                    mEndsSpinner.setSelection(2);
                }
            }
            if (offer.has(ServiceConstants.ID_ATTRIBUTE)) {
                mOfferId = offer.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param date - the expiration date.
     * @return the expiration date in epoch format
     */
    private long getExpirationDateEpoch(Calendar date) {
        return date.getTimeInMillis()/1000;
    }

    @Override
    public void onSuccess() {
        Toast.makeText(UploadNewOfferActivity.this, "SUCCESS", Toast.LENGTH_SHORT).show();
        mUploadButton.setBackgroundColor(
                UploadNewOfferActivity.this.getResources().getColor(R.color.secondaryColor));
        mUploadButton.setClickable(true);
        setResult(UPLOAD_OFFER_RESULT_CODE);
        finish();
    }

    @Override
    public void onFail(String errorMessage) {
        Toast.makeText(UploadNewOfferActivity.this, "FAIL", Toast.LENGTH_SHORT).show();
        mUploadButton.setBackgroundColor(
                UploadNewOfferActivity.this.getResources().getColor(R.color.secondaryColor));
        mUploadButton.setClickable(true);
    }
}
