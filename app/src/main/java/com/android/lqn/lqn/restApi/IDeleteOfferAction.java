package com.android.lqn.lqn.restApi;

/**
 * Created by augustopinto on 8/5/17.
 */

public interface IDeleteOfferAction {

    void onSuccess();

    void onFail(String message);
}
