package com.android.lqn.lqn.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.utils.BasicUtils;
import com.android.lqn.lqn.utils.ServiceConstants;


/**
 * This Dialog shows the LQN score variables in order to check if the score has been calculated
 * properly.
 * Created by augustopinto on 5/3/17.
 */

public class LqnScoreDialog extends Dialog {

    private Context mContext;
    private TextView mDiscountTv;
    private TextView mLqnPointsTv;
    private TextView mNextBuyTv;
    private TextView mStockTv;
    private TextView mTimeTv;
    private TextView mPrevPriceTv;
    private TextView mActualPriceTv;
    private RelativeLayout mPricesContainer;
    private Button mGotItButton;

    public LqnScoreDialog(@NonNull Context context) {
        super(context);
        mContext = context;
        initDialogView();
        initViews();
    }

    private void initDialogView() {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.lqn_score_dialog);
    }

    private void initViews() {
        mDiscountTv = (TextView) findViewById(R.id.discount_tv);
        mLqnPointsTv = (TextView) findViewById(R.id.lqn_points_tv);
        mNextBuyTv = (TextView) findViewById(R.id.next_buy_tv);
        mStockTv = (TextView) findViewById(R.id.stock_tv);
        mPrevPriceTv = (TextView) findViewById(R.id.previous_tv);
        mActualPriceTv = (TextView) findViewById(R.id.actual_tv);
        mTimeTv = (TextView) findViewById(R.id.time_tv);
        mPricesContainer = (RelativeLayout) findViewById(R.id.offer_type_container);
        mGotItButton = (Button) findViewById(R.id.got_it_button);
        mGotItButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void setDiscountValue(String discount) {
        if (mPricesContainer != null && mPricesContainer.getVisibility() == View.VISIBLE) {
            mPricesContainer.setVisibility(View.GONE);
        }
        if (mDiscountTv != null) {
            mDiscountTv.setVisibility(View.VISIBLE);
            mDiscountTv.setText(mContext.getString(R.string.score_dialog_discount, discount));
        }
    }

    private void setActualAndPreviousPrice(String previous, String actual) {
        if (mDiscountTv.getVisibility() == View.VISIBLE) {
            mDiscountTv.setVisibility(View.GONE);
        }
        if (mPrevPriceTv != null && mActualPriceTv != null) {
            mPricesContainer.setVisibility(View.VISIBLE);
            mPrevPriceTv.setText(mContext.getString(R.string.score_dialog_previous, previous));
            mActualPriceTv.setText(mContext.getString(R.string.score_dialog_actual, actual));
        }
    }

    private void setComboValues() {
        mDiscountTv.setVisibility(View.GONE);
        mPricesContainer.setVisibility(View.GONE);
    }

    private void setLqnPointsValue(String lqnPoints) {
        if (mLqnPointsTv != null) {
            mLqnPointsTv.setText(mContext.getString(R.string.score_dialog_lqn_points, lqnPoints));
        }
    }

    private void setNextBuyValue(String nextBuy) {
        if (mNextBuyTv != null) {
            mNextBuyTv.setText(mContext.getString(R.string.score_dialog_money_next_buy, nextBuy));
        }
    }

    private void setStockValue(String stock) {
        if (mStockTv != null) {
            mStockTv.setText(mContext.getString(R.string.score_dialog_stock, stock));
        }
    }

    private void setTimeValue(long time) {
        if (mTimeTv != null) {
            if (time <= 0) {
                mTimeTv.setText(mContext.getString(R.string.score_dialog_time_expired));
            } else {
                mTimeTv.setText(mContext.getString(R.string.score_dialog_time, time + ""));
            }
        }
    }

    private void showOfferTypeContent(Offer offer) {
        switch (offer.getOfferType().getId()) {
            case ServiceConstants.OLD_NEW_ID:
                setActualAndPreviousPrice(String.valueOf(offer.getOfferType().getPreviousPrice()),
                        String.valueOf(offer.getOfferType().getActualPrice()));
                break;
            case ServiceConstants.DISCOUNT_ID:
                setDiscountValue(String.valueOf(offer.getOfferType().getDiscountPercentage()));
                break;
            case ServiceConstants.TWO_FOR_ONE_ID:
                setComboValues();
                break;
        }
    }

    public void showDialog(Offer offer) {
        showOfferTypeContent(offer);
        setStockValue(String.valueOf(offer.getStock()));
        setLqnPointsValue(String.valueOf(offer.getLqnPoints()));
        setNextBuyValue(String.valueOf(offer.getRefoundPercent()));
        setTimeValue(BasicUtils.getExpirationDaysLeft(offer.getTimeLeft()));
        show();
    }

}
