package com.android.lqn.lqn.restApi;

import com.android.lqn.lqn.model.User;
import com.android.lqn.lqn.utils.ServiceConstants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by augustopinto on 5/19/17.
 */

public interface LoginService {

    @GET(ServiceConstants.LOGIN)
    Call<User> login(@Path("username") String username, @Path("password") String password);

}
