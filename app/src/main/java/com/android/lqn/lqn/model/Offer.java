package com.android.lqn.lqn.model;

import android.support.annotation.NonNull;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 *
 * Created by augustopinto on 4/4/17.
 */

public class Offer implements Serializable, Comparable<Offer> {

    @SerializedName(ServiceConstants.ID_ATTRIBUTE)
    private int mId;

    @SerializedName(ServiceConstants.NAME_ATTRIBUTE)
    private String mName;

    @SerializedName(ServiceConstants.DESCRIPTION_ATTRIBUTE)
    private String mDescription;

    @SerializedName(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE)
    private long mExpirationDate;

    @SerializedName(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE)
    private float mRefoundPercent;

    @SerializedName(ServiceConstants.STOCK_ATTRIBUTE)
    private int mStock;

    @SerializedName(ServiceConstants.LQN_POINTS)
    private int mLqnPoints;

    @SerializedName(ServiceConstants.SHOP_ATTRIBUTE)
    private Store mStore;

    @SerializedName(ServiceConstants.OFFER_TYPE_ATTRIBUTE)
    private OfferType mOfferType;

    @SerializedName(ServiceConstants.PAYMENT_METHOD_ATTRIBUTE)
    private PaymentMethod mPaymentMethod;

    @SerializedName(ServiceConstants.IMAGE_URL_ATTRIBUTE)
    private String mImageUrl;

    @SerializedName(ServiceConstants.LQN_SCORE)
    private double mLqnScore;

    @SerializedName(ServiceConstants.AMOUNT_RESERVED)
    private int mAmountReserved;

    @SerializedName(ServiceConstants.HASH_CODE)
    private String mHashCode;

    public Offer() {
        mId = 0;
        mName = "No data";
        mDescription = "No description";
        mExpirationDate = 0;
        mImageUrl = "";
        mRefoundPercent = 0F;
        mStock = 0;
        mLqnPoints = 0;
        mLqnScore = 0;
        mAmountReserved = 0;
        mHashCode = "";
        mStore = new Store();
        mOfferType = new OfferType();
        mPaymentMethod = new PaymentMethod();
    }

    public Offer(JSONObject json) {
        parseToObject(json);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public double getScore() {
        return mLqnScore;
    }

    public void setScore(double score) {
        mLqnScore = score;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public long getTimeLeft() {
        return mExpirationDate;
    }

    public void setTimeLeft(long mTimeLeft) {
        this.mExpirationDate = mTimeLeft;
    }

    public Store getShop() {
        return mStore;
    }

    public void setShop(Store store) {
        mStore = store;
    }

    public OfferType getOfferType() {
        return mOfferType;
    }

    public void setOfferType(OfferType newOfferType) {
        mOfferType = newOfferType;
    }

    public PaymentMethod getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(PaymentMethod payMethod) {
        mPaymentMethod = payMethod;
    }

    public float getRefoundPercent() {
        return mRefoundPercent;
    }

    public void setRefoundPercent(float refoundPercent) {
        this.mRefoundPercent = refoundPercent;
    }

    public int getStock() {
        return mStock;
    }

    public void setStock(int stock) {
        this.mStock = stock;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public int getLqnPoints() {
        return mLqnPoints;
    }

    public void setLqnPoints(int LqnPoints) {
        mLqnPoints = LqnPoints;
    }

    public int getAmountReserved() {
        return mAmountReserved;
    }

    public void setAmountReserved(int amountReserved) {
        mAmountReserved = amountReserved;
    }

    public String getHashCode() {
        return mHashCode;
    }

    public void setHashCode(String hashCode) {
        mHashCode = hashCode;
    }

    public JSONObject parseToJSON() {
        JSONObject json = new JSONObject();
        try {
            json.put(ServiceConstants.ID_ATTRIBUTE, mId);
            json.put(ServiceConstants.NAME_ATTRIBUTE, mName);
            json.put(ServiceConstants.DESCRIPTION_ATTRIBUTE, mDescription);
            json.put(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE, mExpirationDate);
            json.put(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE, mRefoundPercent);
            json.put(ServiceConstants.STOCK_ATTRIBUTE, mStock);
            json.put(ServiceConstants.IMAGE_URL_ATTRIBUTE, mImageUrl);
            json.put(ServiceConstants.LQN_POINTS, mLqnPoints);
            json.put(ServiceConstants.LQN_SCORE, mLqnScore);
            json.put(ServiceConstants.AMOUNT_RESERVED, mAmountReserved);
            json.put(ServiceConstants.HASH_CODE, mHashCode);
            json.put(ServiceConstants.OFFER_TYPE_ATTRIBUTE, mOfferType.parseToJson());
            json.put(ServiceConstants.STORE_OBJ_ATTRIBUTE, mStore.parseToJson());
            json.put(ServiceConstants.PAYMENT_METHOD_ATTRIBUTE, mPaymentMethod.parseToJson());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json;
    }

    private void parseToObject(JSONObject json) {
        try {
            if (json.has(ServiceConstants.ID_ATTRIBUTE)) {
                mId = json.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.NAME_ATTRIBUTE)) {
                mName = json.getString(ServiceConstants.NAME_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.DESCRIPTION_ATTRIBUTE)) {
                mDescription = json.getString(ServiceConstants.DESCRIPTION_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE)) {
                mExpirationDate = json.getLong(ServiceConstants.EXPIRATION_DATE_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE)) {
                mRefoundPercent = (float) json.getDouble(ServiceConstants.REFOUND_PERCENT_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.STOCK_ATTRIBUTE)) {
                mStock = json.getInt(ServiceConstants.STOCK_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.IMAGE_URL_ATTRIBUTE)) {
                mImageUrl = json.getString(ServiceConstants.IMAGE_URL_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.LQN_POINTS)) {
                mLqnPoints = json.getInt(ServiceConstants.LQN_POINTS);
            }
            if (json.has(ServiceConstants.LQN_SCORE)) {
                mLqnScore = json.getDouble(ServiceConstants.LQN_SCORE);
            }
            if (json.has(ServiceConstants.AMOUNT_RESERVED)) {
                mAmountReserved = json.getInt(ServiceConstants.AMOUNT_RESERVED);
            }
            if (json.has(ServiceConstants.HASH_CODE)) {
                mHashCode = json.getString(ServiceConstants.HASH_CODE);
            }
            if (json.has(ServiceConstants.STORE_OBJ_ATTRIBUTE)) {
                mStore = new Store(json.getJSONObject(ServiceConstants.STORE_OBJ_ATTRIBUTE));
            }
            if (json.has(ServiceConstants.PAYMENT_METHOD_ATTRIBUTE)) {
                mPaymentMethod = new PaymentMethod(json.getJSONObject(ServiceConstants.PAYMENT_METHOD_ATTRIBUTE));
            }
            if (json.has(ServiceConstants.OFFER_TYPE_ATTRIBUTE)) {
                mOfferType = new OfferType(json.getJSONObject(ServiceConstants.OFFER_TYPE_ATTRIBUTE));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int compareTo(@NonNull Offer o) {
        return compare(mLqnPoints, o.getLqnPoints());
    }

    private int compare(int x, int y) {
        return (x < y) ? -1 : ((x == y) ? 0 : 1);
    }
}
