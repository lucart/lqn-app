package com.android.lqn.lqn.model;

import com.android.lqn.lqn.utils.ServiceConstants;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by augustopinto on 5/23/17.
 */

public class UserType implements Serializable {

    @SerializedName(ServiceConstants.ID_ATTRIBUTE)
    private int mId;

    @SerializedName(ServiceConstants.NAME_ATTRIBUTE)
    private String mName;

    @SerializedName(ServiceConstants.DESCRIPTION_ATTRIBUTE)
    private String mDescription;

    public UserType(int id, String name, String desc) {
        mId = id;
        mName = name;
        mDescription = desc;
    }

    public UserType() {
        this(-1, "", "");
    }

    public UserType(JSONObject userType) {
        parseToObject(userType);
    }

    public int getId() {
        return mId;
    }

    public void setId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public JSONObject parseToJson() {
        JSONObject userType = new JSONObject();

        try {
            userType.put(ServiceConstants.ID_ATTRIBUTE, mId);
            userType.put(ServiceConstants.DESCRIPTION_ATTRIBUTE, mDescription);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return userType;
    }

    private void parseToObject(JSONObject json) {
        try {
            if (json.has(ServiceConstants.ID_ATTRIBUTE)) {
                mId = json.getInt(ServiceConstants.ID_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.NAME_ATTRIBUTE)) {
                mName = json.getString(ServiceConstants.NAME_ATTRIBUTE);
            }
            if (json.has(ServiceConstants.DESCRIPTION_ATTRIBUTE)) {
                mDescription = json.getString(ServiceConstants.DESCRIPTION_ATTRIBUTE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
