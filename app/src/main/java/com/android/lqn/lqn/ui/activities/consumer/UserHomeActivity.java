package com.android.lqn.lqn.ui.activities.consumer;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.FeedController;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.restApi.IOffersLoaded;
import com.android.lqn.lqn.restApi.IShopActions;
import com.android.lqn.lqn.ui.activities.LoginActivity;
import com.android.lqn.lqn.ui.adapters.consumer.PublicationAdapter;
import com.android.lqn.lqn.ui.dialogs.LqnScoreDialog;
import com.android.lqn.lqn.utils.EndlessRecyclerViewScrollListener;
import com.android.lqn.lqn.utils.ServiceConstants;

import org.json.JSONException;

import java.util.List;

/**
 * Created by augustopinto on 3/12/17.
 */

public class UserHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, IOffersLoaded {

    private static final String TAG = UserHomeActivity.class.getSimpleName();
    private static final int DEFAULT_OFFER_TYPE_ID = 1;

    private Context mContext;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private LqnScoreDialog mLqnScoreDialog;

    private RecyclerView mRecyclerView;
    private PublicationAdapter mAdapter;
    private LinearLayoutManager mLayoutManager;
    private ProgressBar mLoadingProgressBar;
    private TextView mEmptyListMessage;
    private RelativeLayout mFilterButton;
    private Dialog mFilterDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private EndlessRecyclerViewScrollListener mScrollListener;

    private int mFilterSelected;
    private int mOfferTypeId;

    private int mCurrentPage;
    private int mFirstPage;

    public interface OnWantItButtonClickListener {
        void onWantItClicked(int offerId);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_home);
        mContext = getApplicationContext();
        mFilterSelected = 0; // 0 is the default filter.
        mOfferTypeId = DEFAULT_OFFER_TYPE_ID;
        mCurrentPage = getResources().getInteger(R.integer.first_page);
        mFirstPage = getResources().getInteger(R.integer.first_page);
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOffersFromCurrentPage();
    }

    /**
     * Inits the UI Views.
     */
    private void initViews() {
        mLoadingProgressBar = (ProgressBar) findViewById(R.id.loading_progress_bar);
        mEmptyListMessage = (TextView) findViewById(R.id.empty_list_message);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                mScrollListener.resetState();
                mCurrentPage = mFirstPage;
                getOffersFromCurrentPage();
            }
        });

        initActionBar();
        initNavigationDrawer();
        initPublicationList();
        initFilterButton();
    }

    /**
     * Sets the action bar in order to set up the functionality and the design
     */
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    /**
     * Inits the navigation drawer in order to show the options when the user press the icon button
     * or swipe from left to right
     */
    private void initNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.open_nav_drawer, R.string.close_nav_drawer);

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        NavigationView navView = (NavigationView) findViewById(R.id.navigation_view);
        navView.setNavigationItemSelectedListener(this);
    }

    /**
     * Inits and set up the publications list in order to show all the discounts previously
     * requested to the web service.
     */
    private void initPublicationList() {

        mRecyclerView = (RecyclerView) findViewById(R.id.feed_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(mContext);

        mScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mCurrentPage = ++ mCurrentPage;
                Log.d(TAG, "should load more items for page " + page);
                Log.d(TAG, "Getting offers for page " + mCurrentPage);
                Log.d(TAG, "the total items count is " + totalItemsCount);
                getOffersFromCurrentPage();
            }
        };
        mRecyclerView.addOnScrollListener(mScrollListener);

        mRecyclerView.setLayoutManager(mLayoutManager);

        mLqnScoreDialog = new LqnScoreDialog(UserHomeActivity.this);
    }

    /**
     * Retrieves the offers from the web service based on the current page attribute.
     * The offer type depends on mOfferTypeId attribute
     */
    private void getOffersFromCurrentPage() {
        if (mCurrentPage == mFirstPage) {
            mLoadingProgressBar.setVisibility(View.VISIBLE);
            mEmptyListMessage.setVisibility(View.INVISIBLE);
            mRecyclerView.setVisibility(View.INVISIBLE);
        }

        FeedController.getInstance().getOffers(mCurrentPage, mOfferTypeId, this);
    }

    /**
     * Inits the filter button.
     */
    private void initFilterButton() {
        createFilterDialog();
        mFilterButton = (RelativeLayout) findViewById(R.id.filter_container);
        mFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFilterDialog.show();
            }
        });
    }

    /**
     * Logs the user out.
     */
    private void logout() {
        LoginController.getInstance(mContext).logout();
        Intent intent = new Intent(mContext, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * Creates the filter dialog with all the options and features.
     */
    private void createFilterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme).
                setTitle(getResources().getString(R.string.filter_dialog_title))
                .setSingleChoiceItems(getResources().getStringArray(R.array.offer_types),
                        mFilterSelected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                mOfferTypeId = ServiceConstants.OLD_NEW_ID;
                                break;
                            case 1:
                                mOfferTypeId = ServiceConstants.TWO_FOR_ONE_ID;
                                break;
                            case 2:
                                mOfferTypeId = ServiceConstants.DISCOUNT_ID;
                                break;
                        }
                    }
                })
                .setPositiveButton(getResources().getString(R.string.filter_dialog_ok_button),
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mScrollListener.resetState();
                        mCurrentPage = mFirstPage;
                        mFilterSelected = i;
                        getOffersFromCurrentPage();
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.filter_dialog_cancel_button),
                        new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });
        mFilterDialog = builder.create();
    }

    /**
     * Creates the listener that will be executed when the user wants to reserve an offer.
     * @return
     */
    private OnWantItButtonClickListener getOnWantItClickListener() {
        return new OnWantItButtonClickListener() {
            @Override
            public void onWantItClicked(int offerId) {
                try {
                    int userId = LoginController.getInstance(mContext).getUserInfo().getId();
                    FeedController.getInstance().reserveOffer(offerId, userId, new IShopActions() {
                        @Override
                        public void onSuccess() {
                            // TODO This listener is just for debug.
                            Log.d(TAG, "OnSuccess - Offer reserved");
                        }

                        @Override
                        public void onFail(String errorMessage) {
                            // TODO This listener is just for debug.
                            Log.d(TAG, "OnFail - Offer could not be reserved");
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /**
     * Goes to active discounts screen
     */
    private void goToActiveDiscounts() {
        Intent intent = new Intent(this, ActiveDiscountsActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_actives_discounts:
                goToActiveDiscounts();
                mDrawerLayout.closeDrawer(GravityCompat.START);
                break;
            case R.id.nav_logout:
                logout();
                break;
        }
        return false;
    }

    @Override
    public void onSuccess(List<Offer> offers) {
        if (mCurrentPage > mFirstPage) {
            if (mAdapter != null) {
                mAdapter.updateOfferList(offers);
            }
        } else {
            mLoadingProgressBar.setVisibility(View.INVISIBLE);
            if (!offers.isEmpty()) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mAdapter = new PublicationAdapter(mContext, offers, mLqnScoreDialog,
                        PublicationAdapter.OfferType.OFFER_FEED);

                mAdapter.setWantItButtonClickListener(getOnWantItClickListener());
                mRecyclerView.setAdapter(mAdapter);
            } else {
                mEmptyListMessage.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void onFail() {
        if (mCurrentPage == mFirstPage) {
            mLoadingProgressBar.setVisibility(View.INVISIBLE);
            mEmptyListMessage.setVisibility(View.VISIBLE);
        }
        Log.e(TAG, "Fail getting offers.");
    }
}
