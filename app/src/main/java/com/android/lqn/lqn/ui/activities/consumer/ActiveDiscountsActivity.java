package com.android.lqn.lqn.ui.activities.consumer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.lqn.lqn.R;
import com.android.lqn.lqn.controller.FeedController;
import com.android.lqn.lqn.controller.LoginController;
import com.android.lqn.lqn.model.Offer;
import com.android.lqn.lqn.restApi.IOffersLoaded;
import com.android.lqn.lqn.ui.adapters.consumer.PublicationAdapter;
import com.android.lqn.lqn.utils.EndlessRecyclerViewScrollListener;

import org.json.JSONException;

import java.util.List;

/**
 * Created by augustopinto on 12/10/17.
 */

public class ActiveDiscountsActivity extends AppCompatActivity implements IOffersLoaded{

    private static final String TAG = ActiveDiscountsActivity.class.getSimpleName();

    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView mEmptyListMessageTextView;
    private LinearLayoutManager mLayoutManager;
    private PublicationAdapter mAdapter;

    private EndlessRecyclerViewScrollListener mScrollListener;
    private int mCurrentPage;
    private int mFirstPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.active_discounts_activity);
        mFirstPage = getResources().getInteger(R.integer.first_page);
        mCurrentPage = mFirstPage;
        initActionBar();
        initViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getOffers();
    }

    /**
     * This method makes the call to the ws and get all the active offers from the user logged in.
     */
    private void getOffers() {
        try {
            int userId = LoginController.getInstance(getApplicationContext()).getUserInfo().getId();
            FeedController.getInstance().getOffersByUser(mCurrentPage, userId, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Inits all the UI Views that will be used in the Activity.
     */
    private void initViews() {
        mEmptyListMessageTextView = (TextView) findViewById(R.id.no_active_discounts_message);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                mScrollListener.resetState();
                mCurrentPage = mFirstPage;
                getOffers();
            }
        });
        initActiveOffersList();
    }

    private void initActiveOffersList() {
        mRecyclerView = (RecyclerView) findViewById(R.id.discounts_recycler_view);
        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mAdapter = new PublicationAdapter(getApplicationContext(), null,
                PublicationAdapter.OfferType.OFFER_ACTIVE_OFFER);

        mScrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                mCurrentPage = ++ mCurrentPage;
                Log.d(TAG, "should load more items for page " + page);
                Log.d(TAG, "Getting offers for page " + mCurrentPage);
                Log.d(TAG, "the total items count is " + totalItemsCount);
                getOffers();
            }
        };

        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addOnScrollListener(mScrollListener);
    }

    /**
     * Sets the action bar in order to set up the functionality and the design
     */
    private void initActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.active_discounts_title);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().show();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    public void onSuccess(List<Offer> offers) {
        Log.d(TAG, "Active offers: " + offers.size());
        if (mCurrentPage > mFirstPage) {
            if (mAdapter != null) {
                mAdapter.updateOfferList(offers);
            }
        } else {
            if (offers.isEmpty()) {
                mEmptyListMessageTextView.setVisibility(View.VISIBLE);
            } else {
                mEmptyListMessageTextView.setVisibility(View.INVISIBLE);
                mAdapter.setOfferList(offers);
            }
        }
    }

    @Override
    public void onFail() {
        Log.d(TAG, "Active offers failed!");
        mEmptyListMessageTextView.setVisibility(View.VISIBLE);
    }
}
